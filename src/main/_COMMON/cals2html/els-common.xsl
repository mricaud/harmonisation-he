<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:functx="http://www.functx.com"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:saxon="http://saxon.sf.net/"
  exclude-result-prefixes="#all"
  version="2.0"
  xml:lang="en">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Generic XSL functions/templates library used at ELS</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:import href="functx.xsl"/>
  
  <!--===================================================  -->
  <!-- COMMON VAR -->
  <!--===================================================  -->
  
  <xd:doc>
    <xd:desc>
      <xd:p>double/simple quot variable, might be usefull within a concat for example</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:variable name="els:dquot" as="xs:string">
    <xsl:text>"</xsl:text>
  </xsl:variable>
  <xsl:variable name="els:quot" as="xs:string">
    <xsl:text>'</xsl:text>
  </xsl:variable>
  
  <!--===================================================  -->
  <!-- FUNCTIONS -->
  <!--===================================================  -->


  <xd:doc>
    <xd:desc>
      <xd:p>Return the file name from an abolute or a relativ path</xd:p>
      <xd:p>If <xd:ref name="filePath" type="parameter">$filePath</xd:ref> is empty it will retunr an empty string (not an empty sequence)</xd:p>
    </xd:desc>
    <xd:param name="filePath">[String] path of the file (typically string(base-uri())</xd:param>
    <xd:param name="withExt">[Boolean] with or without extension</xd:param>
    <xd:return>File name (with or without extension)</xd:return>
  </xd:doc>
  <xsl:function name="els:getFileName" as="xs:string">
    <xsl:param name="filePath" as="xs:string?"/>
    <xsl:param name="withExt" as="xs:boolean"/>
    <xsl:choose>
      <!-- An empty string would lead an error in the next when (because of a empty regex)-->
      <xsl:when test="normalize-space($filePath) = ''">
        <xsl:value-of select="$filePath"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fileNameWithExt" select="functx:substring-after-last-match($filePath,'/')" as="xs:string?"/>
        <xsl:variable name="fileNameNoExt" select="functx:substring-before-last-match($fileNameWithExt,'\.')" as="xs:string?"/>
        <!-- If the fileName has no extension (ex. : "foo"), els:getFileExt() will return renvoie the file name... which is not what expected
        <xsl:variable name="ext" select="concat('.',els:getFileExt($fileNameWithExt))" as="xs:string"/>-->
        <!-- This works with no extension files -> $ext is an empty string here-->
        <xsl:variable name="ext" select="functx:substring-after-match($fileNameWithExt,$fileNameNoExt)" as="xs:string?"/>
        <xsl:sequence select="concat('', $fileNameNoExt, if ($withExt) then ($ext) else (''))"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>1arg signature of els:getFileName. Default : extension is on</xd:doc>
  <xsl:function name="els:getFileName" as="xs:string">
    <xsl:param name="filePath" as="xs:string?"/>
    <xsl:sequence select="els:getFileName($filePath,true())"/>
  </xsl:function>
  
  
  <xd:doc>Check if the element has a specific class (several class values might be tested at once)</xd:doc>
  <xsl:function name="els:hasClass" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="class" as="xs:string*"/>
    <xsl:sequence select="tokenize($e/@class, '\s+') = $class"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Display any node (element, attribute, text, pi, etc.) as a readable string</xd:p>
    </xd:desc>
    <xd:param name="node">The node to be displayed</xd:param>
    <xd:return>A textual representation of <xd:ref name="$node" type="parameter">$node</xd:ref></xd:return>
  </xd:doc>
  <xsl:function name="els:displayNode" as="xs:string">
    <xsl:param name="node" as="item()"/>
    <xsl:variable name="tmp" as="xs:string*">
      <xsl:choose>
        <xsl:when test="empty($node)">empty_sequence</xsl:when>
        <xsl:when test="$node/self::*">
          <xsl:text>element():</xsl:text>
          <xsl:value-of select="name($node)"/>
          <xsl:if test="$node/@*">
            <xsl:text>_</xsl:text>
          </xsl:if>
          <xsl:for-each select="$node/@*">
            <xsl:sort/>
            <xsl:value-of select="concat('@',name(),'=',$els:dquot,.,$els:dquot,if (position()!=last()) then ('_') else (''))"/>
          </xsl:for-each>
        </xsl:when>
        <!--FIXME : ce test ne marche pas... ?-->
        <xsl:when test="$node/self::text()">
          <xsl:text>text() </xsl:text>
          <xsl:value-of select="substring($node,1,30)"/>
        </xsl:when>
        <xsl:when test="$node/self::attribute()">
          <xsl:value-of select="'attribute_@' || name($node) || '=' || $els:dquot || $node || $els:dquot"/>
        </xsl:when>
        <xsl:when test="$node/self::comment()">
          <xsl:text>comment() </xsl:text>
          <xsl:value-of select="substring($node,1,30)"/>
        </xsl:when>
        <xsl:when test="$node/self::processing-instruction()">
          <xsl:text>processing-instruction() </xsl:text>
          <xsl:value-of select="substring($node,1,30)"/>
        </xsl:when>
        <xsl:when test="$node/self::document-node()">
          <xsl:text>document-node() </xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>unrecognized node type</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="string-join($tmp,'')"/>
  </xsl:function>
  
  
  <xd:doc>Get the full XML path of any node in an XML with position predicates([n])
    cf. http://www.xsltfunctions.com/xsl/functx_path-to-node-with-pos.html
  </xd:doc>
  <xsl:template match="*" name="els:get-xpath" mode="get-xpath">
    <xsl:param name="node" select="." as="node()"/>
    <xsl:param name="nsprefix" select="''" as="xs:string"/>
    <xsl:param name="display_position" select="true()" as="xs:boolean"/>
    <xsl:variable name="result" as="xs:string*">
      <xsl:for-each select="$node/ancestor-or-self::*">
        <xsl:variable name="id" select="generate-id(.)" as="xs:string"/>
        <xsl:variable name="name" select="name()" as="xs:string"/>
        <xsl:choose>
          <xsl:when test="not(contains($name,':'))">
            <xsl:value-of select="concat('/',if ($nsprefix!='') then (concat($nsprefix,':')) else(''), $name)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('/', $name)"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:for-each select="../*[name() = $name]">
          <xsl:if test="generate-id(.)=$id and $display_position">
            <!--FIXME : add position() != 1 to get rid of unusfull "[1]" predicates-->
            <xsl:text>[</xsl:text>
            <xsl:value-of select="format-number(position(),'0')"/>
            <xsl:text>]</xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:if test="not($node/self::*)">
        <xsl:value-of select="concat('/@',name($node))"/>
      </xsl:if>
    </xsl:variable>
    <xsl:value-of select="string-join($result, '')"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>els:get-xpath return full XML path of the current node</xd:p>
      <xd:p>If saxon:path() is available then it will be used, else we will use the template "els:get-xpath"</xd:p>
    </xd:desc>
    <xd:param name="node">[Node] Node we wan the XML path</xd:param>
    <xd:return>XML path of $node</xd:return>
  </xd:doc>
  <xsl:function name="els:get-xpath" as="xs:string">
    <xsl:param name="node" as="node()"/>
    <xsl:choose>
      <xsl:when test="function-available('saxon:path')">
        <xsl:value-of select="saxon:path($node)" use-when="function-available('saxon:path')"/>
        <!--To avoid a saxon warning at compilation time, we plan the case (impossible in this when) of the "inverse" use-when 
        (If not, Saxon will warng of a condition brnanch that could return an empty seq instead of a string-->
        <xsl:value-of select="'This will never happen here'" use-when="not(function-available('saxon:path'))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="xpath" as="xs:string">
          <xsl:call-template name="els:get-xpath">
            <xsl:with-param name="node" select="$node" as="node()"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$xpath"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>els:get-xpath with more arguments (call for template els:get-xpath instead of saxon:path)</xd:p>
    </xd:desc>
    <xd:param name="node">[Node] node to get the XML path</xd:param>
    <xd:param name="nsprefix">Adding a prefixe on each path item</xd:param>
    <xd:param name="display_position">Diplay position predicate for each item of the path</xd:param>
    <xd:return>XML path of the $node formated as indicated with $nsprefix and $display_position</xd:return>
  </xd:doc>
  <xsl:function name="els:get-xpath" as="xs:string">
    <xsl:param name="node" as="node()"/>
    <xsl:param name="nsprefix" as="xs:string"/>
    <xsl:param name="display_position" as="xs:boolean"/>
    <xsl:variable name="xpath" as="xs:string">
      <xsl:call-template name="els:get-xpath">
        <xsl:with-param name="node" select="$node" as="node()"/>
        <xsl:with-param name="nsprefix" select="$nsprefix" as="xs:string"/>
        <xsl:with-param name="display_position" select="$display_position" as="xs:boolean"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="string-join(tokenize($xpath,'/'),'/')"/>
  </xsl:function>
  
</xsl:stylesheet>