<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:he="http://www.lefebvre-sarrut.eu/ns/hubeditorial"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Convertis les "Fiche OASIS Dalloz" au format harmonisé pour le Hub Editorial</xd:p>
      <xd:p>ANALYSE/QUESTIONS : https://confluence.els-gestion.eu/display/ML/Harmonisation+%3A+Dalloz+Fiches+OASIS</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:import href="../he-harmonize-common.xsl"/>
  
  <xsl:output method="xml"/>
  
  <xsl:variable name="ID" as="xs:string" select="concat(/OASIS/FICHE/METAS/ID, '-', els:normalize-ID_LOG(/OASIS/FICHE/METAS/ID_LOG))"/>
  
  <!--=========================================================================-->
  <!--INIT-->
  <!--=========================================================================-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="he:harmonize-dz-oasis.main"/>
  </xsl:template>
  
  <!--=========================================================================-->
  <!--MAIN-->
  <!--=========================================================================-->
  
  <xsl:template match="/" mode="he:harmonize-dz-oasis.main">
    <xsl:if test="count(OASIS/FICHE) != 1">
      <xsl:sequence select="he:schematron-check(concat('Found', count(FICHE), ' FICHE'))"/>
    </xsl:if>
    <xsl:apply-templates select="OASIS/FICHE" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="FICHE" mode="he:harmonize-dz-oasis.main">
    <!--FIXME : @about = URI (logique) du doc dans le HE-->
    <html about="{$ID}.xml" typeof="frbr:Expression"
      prefix="
      frbr: http://purl.org/vocab/frbr/core#
      he: http://www.lefebvre-sarrut.eu/ns/hubeditorial
      dc: http://purl.org/dc/elements/1.1/
      dcterms: http://purl.org/dc/terms/
      taxo: http://lefebvre-sarrut.eu/ns/taxonomy/
      ">
      <head>
        <meta property="frbr:classification" content="default"/>
        <meta property="he:doctype" content="DZ_TEE_ficheOasis"/>
        <xsl:apply-templates select="METAS/*" mode="he:harmonize-dz-oasis.metadata"/>
      </head>
      <body>
        <article>
          <xsl:apply-templates select="METAS/INTITULE" mode="he:harmonize-dz-oasis.content"/>
          <xsl:apply-templates select="* except METAS" mode="he:harmonize-dz-oasis.content"/>
        </article>
      </body>
    </html>
  </xsl:template>
  
  
  <!--=========================================================================-->
  <!--METADATA-->
  <!--=========================================================================-->
  
  <xsl:template match="node() | @*" mode="he:harmonize-dz-oasis.metadata">
    <xsl:copy>
      <xsl:message>UNMATCHED node mode="he:harmonize-dz-oasis.metadata" : <xsl:value-of select="saxon:path()"/></xsl:message>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="METAS/INTITULE" mode="he:harmonize-dz-oasis.metadata">
    <title><xsl:value-of select="normalize-space(.)"/></title>
  </xsl:template>
 
  <!--===========================-->
  <!--IDENTIFIER-->
  <!--===========================-->
  
  <xsl:template match="METAS/ID" mode="he:harmonize-dz-oasis.metadata">
    <meta property="dc:identifier" content="{.}"/>
  </xsl:template>
  
  <xsl:template match="METAS/ID_LOG" mode="he:harmonize-dz-oasis.metadata">
    <meta property="dc:identifier" role="{lower-case(local-name())}" content="{.}"/>
  </xsl:template>
  
  <!--===========================-->
  <!--DATES-->
  <!--===========================-->
  
  <xsl:template match="METAS/DATES" mode="he:harmonize-dz-oasis.metadata">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="METAS/DATES/DATE-CREA" mode="he:harmonize-dz-oasis.metadata">
    <xsl:if test="not(matches(., '\d\d\d\d-\d\d-\d\d'))">
      <xsl:variable name="msg" as="xs:string*"><xsl:value-of select="local-name()"/>="<xsl:value-of select="."/>" n'a pas le bon format</xsl:variable>
      <xsl:sequence select="he:schematron-check($msg)"/>
    </xsl:if>
    <meta property="dc:date" role="{lower-case(local-name())}" content="{.}"/>
  </xsl:template>
  
  <xsl:template match="METAS/DATES/DATE-MAJ" mode="he:harmonize-dz-oasis.metadata">
    <xsl:if test="not(matches(., '\d\d\d\d-\d\d-\d\d'))">
      <xsl:variable name="msg" as="xs:string*"><xsl:value-of select="local-name()"/>="<xsl:value-of select="."/>" n'a pas le bon format</xsl:variable>
      <xsl:sequence select="he:schematron-check($msg)"/>
    </xsl:if>
    <meta property="dc:date" role="{lower-case(local-name())}" content="{.}"/>
  </xsl:template>
  
  <xsl:template match="METAS/DATES/DATE-PUBLI" mode="he:harmonize-dz-oasis.metadata">
    <xsl:if test="not(matches(., '\d\d\d\d-\d\d-\d\d'))">
      <xsl:variable name="msg" as="xs:string*"><xsl:value-of select="local-name()"/>="<xsl:value-of select="."/>" n'a pas le bon format</xsl:variable>
      <xsl:sequence select="he:schematron-check($msg)"/>
    </xsl:if>
    <meta property="dc:date" role="{lower-case(local-name())}" content="{.}"/>
  </xsl:template>
  
  <!--===========================-->
  <!--MATIERES-->
  <!--===========================-->
  
  <xsl:template match="METAS/MATIERES" mode="he:harmonize-dz-oasis.metadata">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="METAS/MATIERES/MATIERE" mode="he:harmonize-dz-oasis.metadata">
    <xsl:if test="not(count(*) = 1 and THM-LIB)">
      <xsl:variable name="msg" as="xs:string*"><xsl:value-of select="local-name()"/> avec autre chose que THM-LIB, non géré</xsl:variable>
      <xsl:sequence select="he:schematron-check($msg)"/>
    </xsl:if>
    <link rel="dc:legalArea" href="taxo:DZ_Matieres#{THM-LIB}" title="{THM-LIB}"/>
  </xsl:template>
  
  
  <!--===========================-->
  <!--THEMES (widgets)-->
  <!--===========================-->
  
  <xsl:template match="METAS/THEMES" mode="he:harmonize-dz-oasis.metadata">
    <xsl:apply-templates select=".//THM-LIB[not(../*/THM-LIB)]" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="THM-LIB[ancestor::THEMES][not(../*/THM-LIB)]" mode="he:harmonize-dz-oasis.metadata">
    <xsl:variable name="THEMES" as="element()" select="ancestor::THEMES"/>
    <xsl:variable name="THM-cascade-elements" as="element()+" select="$THEMES//THM-LIB[. &lt;&lt; current()] | current()"/>
    <xsl:variable name="THM-cascade-string" select="string-join($THM-cascade-elements/text(), ' > ')"/>
    <link rel="dc:subject" role="theme" href="taxo:DZ_Themes#{$THM-cascade-string}" title="{$THM-cascade-string}">
      <xsl:if test="$THEMES/@CONTEXTE">
        <xsl:attribute name="data-product-scope" select="$THEMES/@CONTEXTE"/>
      </xsl:if>
    </link>
  </xsl:template>
  
  <!--===========================-->
  <!--THEMATISATION (Hulk)-->
  <!--===========================-->
  
  <xsl:template match="METAS/THEMATISATION" mode="he:harmonize-dz-oasis.metadata">
    <xsl:apply-templates select=".//THM-LIB[not(../*/THM-LIB)]" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="THM-LIB[ancestor::THEMATISATION][not(../*/THM-LIB)]" mode="he:harmonize-dz-oasis.metadata">
    <xsl:variable name="THEMATISATION" as="element()" select="ancestor::THEMATISATION"/>
    <xsl:variable name="THM-cascade-elements" as="element()+" select="$THEMATISATION//THM-LIB[. &lt;&lt; current()] | current()"/>
    <xsl:variable name="THM-cascade-string" select="string-join($THM-cascade-elements/text(), ' > ')"/>
    <link rel="dc:subject" role="thematisation" href="taxo:DZ_Thematisation#{$THM-cascade-string}" title="{$THM-cascade-string}"/>
  </xsl:template>
  
  <!--===========================-->
  <!--CIBLES (commerciales)-->
  <!--===========================-->
  
  <xsl:template match="METAS/CIBLES" mode="he:harmonize-dz-oasis.metadata">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="METAS/CIBLES/CIBLE" mode="he:harmonize-dz-oasis.metadata">
    <link rel="dcterms:audience" href="taxo:DZ_audience#{@ID}" title="{@ID}"/>
  </xsl:template>
  
  <!--=========================================================================-->
  <!--CONTENT-->
  <!--=========================================================================-->
  
  <xsl:template match="node() | @*" mode="he:harmonize-dz-oasis.content">
    <xsl:copy>
      <xsl:message>UNMATCHED node mode="he:harmonize-dz-oasis.content" : <xsl:value-of select="saxon:path()"/></xsl:message>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="METAS/INTITULE" mode="he:harmonize-dz-oasis.content">
    <h1 class="els-content-title"><xsl:apply-templates mode="#current"/></h1>
  </xsl:template>
  
  <xsl:template match="DEFINITION" mode="he:harmonize-dz-oasis.content">
    <div class="els-block {lower-case(local-name())}">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </div>
  </xsl:template>
  
  <xsl:template match="P" mode="he:harmonize-dz-oasis.content">
    <p>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </p>
  </xsl:template>
  
  <xsl:template match="RENVOIS-TEXTES | RENVOIS-DECISIONS | RENVOIS-REVUES | RENVOIS-OASIS" mode="he:harmonize-dz-oasis.content">
    <nav class="{lower-case(local-name())}">
      <ul class="els-link-list">
        <xsl:apply-templates select="@* | node()" mode="#current"/>
      </ul>
    </nav>
  </xsl:template>
  
  <xsl:template match="RENVOIS-BND | RENVOIS-ENCYCLO" mode="he:harmonize-dz-oasis.content">
    <nav class="{lower-case(local-name())}">
      <xsl:apply-templates mode="#current"/>
    </nav>
  </xsl:template>
  
  <xsl:template match="RENVOI-COLLEC" mode="he:harmonize-dz-oasis.content">
    <xsl:if test="COLLECTION">
      <h4><xsl:apply-templates select="COLLECTION/node()" mode="#current"/></h4>
      <ul class="els-links-list">
        <xsl:apply-templates select="* except COLLECTION" mode="#current"/>
      </ul>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="RENVOI-ENCYCLO" mode="he:harmonize-dz-oasis.content">
    <xsl:if test="REPERTOIRE">
      <h4><xsl:apply-templates select="REPERTOIRE/node()" mode="#current"/></h4>
      <ul class="els-links-list">
        <xsl:apply-templates select="* except REPERTOIRE" mode="#current"/>
      </ul>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="RENV-CODE | RENV-LOI | RENV-JURIS | RENV-RUBR | RENV-ARTI | RENV-OUVRAGE | RENV-FICHE" mode="he:harmonize-dz-oasis.content">
    <li>
      <!--FIXME : <span class="els-link"> à remplacer par <cite>-->
      <cite class="{lower-case(local-name())}">
        <xsl:apply-templates mode="#current"/>
      </cite>
    </li>
  </xsl:template>
  
  <xsl:template match="BIBLIO" mode="he:harmonize-dz-oasis.content">
    <section class="{lower-case(local-name())}">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </section>
  </xsl:template>
  
  <xsl:template match="TEXTE" mode="he:harmonize-dz-oasis.content">
    <!--<main>-->
      <xsl:apply-templates mode="#current"/>
    <!--</main>-->
  </xsl:template>
  
  <xsl:template match="NIV1 | NIV2 | NIV3 | NIV4 | NIV5" mode="he:harmonize-dz-oasis.content">
    <xsl:variable name="level" as="xs:string" select="replace(local-name(), 'NIV', '')"/>
    <xsl:element name="h{$level}">
      <xsl:attribute name="class" select="concat('els-heading-', $level)"/>
      <xsl:if test="TIT-NIV/NUM-NIV">
        <xsl:attribute name="data-els-num" select="TIT-NIV/NUM-NIV"/>
      </xsl:if>
      <xsl:apply-templates select="TIT-NIV/LIB-NIV/node()" mode="#current"/>
    </xsl:element>
    <xsl:apply-templates select="* except TIT-NIV" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="TXT-NIV" mode="he:harmonize-dz-oasis.content">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="@ID" mode="he:harmonize-dz-oasis.content">
    <xsl:attribute name="id" select="."/>
  </xsl:template>
  
  <!--===========================-->
  <!--LINKS -->
  <!--===========================-->
  
  <xsl:template match="REF[@URI or @REFID]" mode="he:harmonize-dz-oasis.content">
    <a class="{lower-case(local-name())}">
      <xsl:attribute name="href">
        <xsl:choose>
          <xsl:when test="@URI">
            <xsl:sequence select="@URI"/>
          </xsl:when>
          <xsl:when test="@REFID">
            <xsl:sequence select="@REFID"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates select="@* except (@URI | @REFID) | node()" mode="#current"/>
    </a>
  </xsl:template>
  
  <xsl:template match="REF[not(@URI or @REFID)]" mode="he:harmonize-dz-oasis.content">
    <span class="{lower-case(local-name())}"><!--fixme-->
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </span>
  </xsl:template>
  
  <xsl:template match="REF/@TYPE" mode="he:harmonize-dz-oasis.content">
    <xsl:attribute name="data-dalloz-type" select="."/>
  </xsl:template>
  
  <xsl:template match="TIT[@URL or @REFID]" mode="he:harmonize-dz-oasis.content">
    <a class="{lower-case(local-name())}">
      <xsl:attribute name="href">
        <xsl:choose>
          <xsl:when test="@URL">
            <xsl:sequence select="@URL"/>
          </xsl:when>
          <xsl:when test="@REFID">
            <xsl:sequence select="concat('/FIXME/', @REFID)"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates select="@* except (@URL | @REFID) | node()" mode="#current"/>
    </a>
  </xsl:template>
  
  <xsl:template match="TIT[not(@URL or @REFID)]" mode="he:harmonize-dz-oasis.content">
    <span class="{lower-case(local-name())}"><!--fixme-->
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </span>
  </xsl:template>
  
  <!--===========================-->
  <!--TYPO -->
  <!--===========================-->
  <!--DTD : GRAS | ITAL | ROMA | EXP | IND | MEV | BR" -->
  
  <!--Aucune occurence trouvées-->
  <xsl:template match="MEV" mode="he:harmonize-dz-oasis.content">
    <span class="{lower-case(local-name())}"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <xsl:template match="GRAS" mode="he:harmonize-dz-oasis.content">
    <strong><xsl:apply-templates mode="#current"/></strong>
  </xsl:template>
  
  <xsl:template match="ITAL" mode="he:harmonize-dz-oasis.content">
    <em><xsl:apply-templates mode="#current"/></em>
  </xsl:template>
  
  <xsl:template match="ROMA" mode="he:harmonize-dz-oasis.content">
    <span class="els-roman"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <xsl:template match="EXP" mode="he:harmonize-dz-oasis.content">
    <sup><xsl:apply-templates mode="#current"/></sup>
  </xsl:template>
  
  <xsl:template match="IND" mode="he:harmonize-dz-oasis.content">
    <sub><xsl:apply-templates mode="#current"/></sub>
  </xsl:template>
  
  <xsl:template match="BR" mode="he:harmonize-dz-oasis.content">
    <br/>
  </xsl:template>
  
  <!--Possible parents : OUVRAGE, RENV-ARTI, RENV-CHAP, RENV-ETUDE, RENV-FORM, RENV-OUVRAGE, RENV-RUBR-->
  <!--<xsl:template match="AUT" mode="he:harmonize-dz-oasis.content">
    <span about="" property="dc:creator"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>-->
  
  <xsl:template match="AUT" mode="he:harmonize-dz-oasis.content">
    <span class="{lower-case(local-name())}"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <!--Possible parents : OUVRAGE, RENV-ARTI, RENV-CHAP, RENV-ETUDE, RENV-FICHE, RENV-FORM, RENV-OUVRAGE, RENV-RUBR-->
  <xsl:template match="TIT" mode="he:harmonize-dz-oasis.content">
    <span class="{lower-case(local-name())}"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <!--Possible parents : OUVRAGE, RENV-ARTI, RENV-ETUDE, RENV-FICHE, RENV-FORM, RENV-JURIS, RENV-LOI, RENV-OUVRAGE, RENV-RUBR-->
  <xsl:template match="DATE" mode="he:harmonize-dz-oasis.content">
    <span class="{lower-case(local-name())}"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <!--=========================================================================-->
  <!--COMMON-->
  <!--=========================================================================-->
  
  <xsl:function name="els:normalize-ID_LOG" as="xs:string">
    <xsl:param name="s" as="xs:string"/>
    <xsl:sequence select="lower-case(els:normalize-no-diacritic(replace($s, '\p{Z}', '_')))"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Normalize the string: remove diacritic marks.</xd:p>
      <xd:p>Example: els:normalize-no-diacritic('éêèàœç')='eeeaœc'</xd:p>
    </xd:desc>
    <xd:param name="string"/>
    <xd:return>the <xd:b>string</xd:b> normalized</xd:return>
  </xd:doc>
  <xsl:function name="els:normalize-no-diacritic" as="xs:string">
    <xsl:param name="s" as="xs:string"/>
    <xsl:sequence select="replace(normalize-unicode($s, 'NFD'), '[\p{M}]', '')"/>
  </xsl:function>
  
</xsl:stylesheet>