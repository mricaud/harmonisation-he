<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:he="http://www.lefebvre-sarrut.eu/ns/hubeditorial"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Convertis les "Fiche FOCUS EFL" au format harmonisé pour le Hub Editorial</xd:p>
    </xd:desc>
  </xd:doc>


  <xsl:function name="he:schematron-check" as="processing-instruction()">
    <xsl:param name="message" as="xs:string*"/>
    <xsl:param name="level" as="xs:string*"/>
    <xsl:processing-instruction name="schematron-{$level}"><xsl:value-of select="$message"/></xsl:processing-instruction>
    <xsl:message>[<xsl:value-of select="upper-case($level)"/>] <xsl:value-of select="$message"/></xsl:message>
  </xsl:function>
  
  <xsl:function name="he:schematron-check" as="processing-instruction()">
    <xsl:param name="message" as="xs:string*"/>
    <xsl:sequence select="he:schematron-check($message, 'error')"/>
  </xsl:function>
  
</xsl:stylesheet>