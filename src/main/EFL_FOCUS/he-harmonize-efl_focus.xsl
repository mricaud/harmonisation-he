<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:he="http://www.lefebvre-sarrut.eu/ns/hubeditorial"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xslLib="http://www.lefebvre-sarrut.eu/ns/els/xslLib"
  xmlns:cals="http://docs.oasis-open.org/ns/oasis-exchange/table"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:juri="http://www.lefebvre-sarrut.eu/ns/juri"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Convertis les "Fiche FOCUS EFL" au format harmonisé pour le Hub Editorial</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:import href="../he-harmonize-common.xsl"/>
  <xsl:import href="../_COMMON/cals2html/cals2html.xsl"/>
  
  
  <xsl:output method="xml"/>
  
  <xsl:variable name="ID" as="xs:string" select="'fixme'"/>
  
  <!--=========================================================================-->
  <!--INIT-->
  <!--=========================================================================-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="he:harmonize-efl-focus.main"/>
  </xsl:template>
  
  <!--=========================================================================-->
  <!--MAIN-->
  <!--=========================================================================-->
  
  <xsl:template match="/" mode="he:harmonize-efl-focus.main">
    <xsl:if test="count(DYNAMEM/*) != 1 
      or count(DYNAMEM/MEM/*) != 1 
      or count(DYNAMEM/MEM/CORPS) != 1
      or count(DYNAMEM/MEM/CORPS/*) != 1 
      or count(DYNAMEM/MEM/CORPS/PART) != 1 
      or count(DYNAMEM/MEM/CORPS/PART/*[not(local-name() = ('INTR','TPART'))]) != 1 
      or count(DYNAMEM/MEM/CORPS/PART/CHAP) != 1">
      <xsl:variable name="msg" as="xs:string">Structure DYNAMEM/MEM/CORPS/PART/CHAP inattendue</xsl:variable>
      <xsl:sequence select="he:schematron-check($msg)"/>
    </xsl:if>
    <!--La fiche proprement dite commence au niveau du chapitre (unique)-->
    <xsl:apply-templates select="DYNAMEM/MEM/CORPS/PART/CHAP" mode="#current"/>
  </xsl:template>
  
  <xsl:template match="CHAP" mode="he:harmonize-efl-focus.main">
    <!--FIXME : @about = URI (logique) du doc dans le HE-->
    <html about="{$ID}.xml" typeof="frbr:Expression"
      prefix="
      frbr: http://purl.org/vocab/frbr/core#
      he: http://www.lefebvre-sarrut.eu/ns/hubeditorial
      dc: http://purl.org/dc/elements/1.1/
      dcterms: http://purl.org/dc/terms/
      taxo: http://lefebvre-sarrut.eu/ns/taxonomy/
      ">
      <head>
        <meta property="frbr:classification" content="default"/>
        <meta property="he:doctype" content="EFL_TEE_ficheFocus"/>
        <meta property="dc:identifier" content="{$ID}"/>
        <xsl:apply-templates select="." mode="he:harmonize-efl-focus.metadata"/>
      </head>
      <body>
        <xsl:apply-templates select="." mode="he:harmonize-efl-focus.content"/>
      </body>
    </html>
  </xsl:template>
  
  
  <!--=========================================================================-->
  <!--METADATA-->
  <!--=========================================================================-->
  
  <xsl:template match="*" mode="he:harmonize-efl-focus.metadata" priority="-1"/>
  
  <xsl:template match="CHAP" mode="he:harmonize-efl-focus.metadata">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="TCHAP" mode="he:harmonize-efl-focus.metadata">
    <title><xsl:value-of select="normalize-space(.)"/></title>
  </xsl:template>
  
  <xsl:template match="CHAP/INTR" mode="he:harmonize-efl-focus.metadata">
    <meta property="dc:identifier" role="intr" content="{.}"/>
  </xsl:template>
 
  <xsl:template match="DATEFICHE" mode="he:harmonize-efl-focus.metadata">
    <meta property="dc:date" content="{.}"/>
  </xsl:template>
  
  <!--=========================================================================-->
  <!--CONTENT-->
  <!--=========================================================================-->
  
  <xsl:template match="node() | @*" mode="he:harmonize-efl-focus.content" priority="-1">
    <xsl:copy>
      <xsl:message>UNMATCHED node mode="he:harmonize-efl-focus.content" : <xsl:value-of select="saxon:path()"/></xsl:message>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="CHAP" mode="he:harmonize-efl-focus.content">
    <article class="els-content efl-actu-focus chap">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </article>
  </xsl:template>
  
  <xsl:template match="CHAP/INTR" mode="he:harmonize-efl-focus.content"/>
  
  <xsl:template match="TCHAP" mode="he:harmonize-efl-focus.content">
    <h1 class="els-content-title">
      <xsl:apply-templates mode="#current"/>
    </h1>
  </xsl:template>
  
  <xsl:template match="DATEFICHE" mode="he:harmonize-efl-focus.content"/>
  
  <!--=======================================-->
  <!--NIVEAUX-->
  <!--=======================================-->
  
  <!--NIV dans les données (dand cet ordre d'importance) : C, D E, H, I-->
  <!--On ne s'aligne pas forcément avec les memento mais plutôt sur un format harmonisé d'actu (dalloz, efl, el),
  donc on ne cherche pas à reprendre tous les niveaux possible DYNAMEM--> 

  <xsl:template match="NIVC | NIVD | NIVE | NIVH | NIVI" mode="he:harmonize-efl-focus.content">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="NIVC/TC" mode="he:harmonize-efl-focus.content">
    <h1 class="els-heading-1">
      <xsl:apply-templates select="parent::*/@*" mode="#current"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </h1>
  </xsl:template>
  
  <xsl:template match="NIVD/TD" mode="he:harmonize-efl-focus.content">
    <h2 class="els-heading-2 {lower-case(local-name())}">
      <xsl:apply-templates select="parent::*/@*" mode="#current"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </h2>
  </xsl:template>
  
  <xsl:template match="NIVE/TE" mode="he:harmonize-efl-focus.content">
    <h3 class="els-heading-3 {lower-case(local-name())}">
      <xsl:apply-templates select="parent::*/@*" mode="#current"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </h3>
  </xsl:template>
  
  <xsl:template match="NIVH/TH" mode="he:harmonize-efl-focus.content">
    <h4 class="els-heading-4 {lower-case(local-name())}">
      <xsl:apply-templates select="parent::*/@*" mode="#current"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </h4>
  </xsl:template>
  
  <xsl:template match="NIVI/TI" mode="he:harmonize-efl-focus.content">
    <h5 class="els-heading-5 {lower-case(local-name())}">
      <xsl:apply-templates select="parent::*/@*" mode="#current"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </h5>
  </xsl:template>
  
  <!--=======================================-->
  <!--PNUM-->
  <!--=======================================-->
  
  <!--Dans les fiches FOCUS, les Pnum ont toujours un numéro vide. 2 possibilités : 
    - utiliser une <section> pour conserver l'id notamment, mais ça rajoute de l'arbo inutile et ce n'est pas aligné avec le reste des actu.
    - applatir les Pnum et conserver l'id au niveau du 1er élément rencontré (hors NO vide)-->
  
  <!--<!ELEMENT P 	%STARTENDTAG; (NO, FOND*, RATTACH*, RDISNOUV*, (%M.P;)*, NIV*, NIVI*)>-->
  <!--<!ENTITY % M.P 	"AL|LST|RTAB|TABLEAU|RFIG|PC|PRECI|EX|ANM|CADRE|math|ILL|ACTE|STABILO|SAVOIR|CONSEIL|ATTENTION">-->
  
  <xsl:template match="P" mode="he:harmonize-efl-focus.content">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--reporte P/@ID sur le 1er enfant-->
  <xsl:template match="P[@ID][normalize-space(NO) = '']/*[not(self::NO)][1]" mode="he:harmonize-efl-focus.content">
    <xsl:variable name="element-with-p-id" as="element()">
      <xsl:copy>
        <xsl:copy-of select="../@ID"/>
        <xsl:copy-of select="node()"/>
      </xsl:copy>
    </xsl:variable>
    <xsl:apply-templates select="$element-with-p-id" mode="#current"/>
  </xsl:template>
  
  <!--Suppression de NO vides-->
  <xsl:template match="NO[normalize-space(.) = '']" mode="he:harmonize-efl-focus.content"/>
  
  <!--=======================================-->
  <!--AL-->
  <!--=======================================-->
  
  <xsl:template match="AL" mode="he:harmonize-efl-focus.content">
    <xsl:variable name="self" as="element()" select="."/>
    <xsl:if test="*[1]/local-name(.) = ('LST', 'TABLEAU') and @ID">
      <xsl:variable name="msg" as="xs:string*">AL qui commence par LST ou TABLEAU, la copie de l'ID du P parent peut ne pas être correctement réalisée</xsl:variable>
      <xsl:sequence select="he:schematron-check($msg)"/>
    </xsl:if>
    <!--On sort les listes et tableaux des alinéas car cela créerait des structures HTML invalide (ul/table sont de type block, interdit dans contenu inline de p)-->
    <!--Plutôt que select="node()" qui prendrait chaque noeud texte d'indentation, on sélectionne le text() non vide et les éléments sous AL-->
    <xsl:for-each-group select="text()[normalize-space()] | *" group-adjacent="local-name() = ('LST', 'TABLEAU')">
      <xsl:choose>
        <xsl:when test="local-name(current-group()[1]) = ('LST', 'TABLEAU')">
          <xsl:apply-templates select="current-group()" mode="#current"/>
        </xsl:when>
        <xsl:otherwise>
          <p>
            <!--on ajoute l'id qu'une fois : sur le 1er group -->
            <xsl:if test="current-group()[1] is $self/node()[self::* or self::text()[normalize-space(.)]][1]">
              <xsl:apply-templates select="$self/@ID" mode="#current"/>
            </xsl:if>
            <xsl:apply-templates select="current-group()" mode="#current"/>
          </p>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each-group> 
  </xsl:template>
  
  <!--Exception pour éviter warning schematron ci-dessus-->
  <!--<xsl:template match="AL[TABLEAU and count(*) = 1 and not(text()[normalize-space(.)])]" mode="he:harmonize-efl-focus.content">
    
  </xsl:template>-->
  
  <!--=======================================-->
  <!--LISTES-->
  <!--=======================================-->
  <!--<!ATTLIST LST PRES 
  (ALPHAM|ALPHAG|ALPHAPAR|ALPHAPARGR|
  NOM|NOPOINTGR|NOTIRGR|NOGR|NOO|NOOGR|NOPAR|NOPARGR|
  PUCE|TIR|AUTRE|2COL|3COL|RIEN) "TIR">-->
  
  <!--TYPE DE PRESENTATION D'UNE LISTE
        ALPHAPAR = A) MAIGRE 
        ALPHAPARGR = A) GRAS
        NOPAR = 1) MAIGRE
        NOPARGR = 1) GRAS
        NOO = 1O MAIGRE
        NOOGR = 1O GRAS
        RIEN = PAS D'INTRODUCTEUR GENERE ; UTILISER EVENTUELLEMENT UN INTRODUCTEUR SAISI -->
  <!--Trouvé dans les données : NOPARGR, ALPHAG, NOO, NOOGR, PUCE-->
  
  <xsl:template match="LST[@PRES = ('TIR', 'PUCE', 'RIEN') or not(@PRES)]" mode="he:harmonize-efl-focus.content">
    <ul>
      <xsl:apply-templates select="@* except @PRES" mode="#current"/>
      <xsl:choose>
        <xsl:when test="@PRES = 'TIR'">
          <xsl:attribute name="data-els-type" select="'dash'"/>
        </xsl:when>
        <xsl:when test="@PRES = 'PUCE'">
          <xsl:attribute name="data-els-type" select="'circle'"/>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates mode="#current"/>
    </ul>
  </xsl:template>
  
  <xsl:template match="LST[matches(@PRES, '(ALPHA|NO)')]" mode="he:harmonize-efl-focus.content">
    <ol>
      <xsl:apply-templates select="@* except @PRES" mode="#current"/>
      <xsl:choose>
        <xsl:when test="starts-with(@PRES, 'ALPHAPAR')">
          <xsl:attribute name="data-els-type" select="'lower-alpha'"/>
          <xsl:attribute name="data-els-sep" select="')'"/>
        </xsl:when>
        <xsl:when test="@PRES = 'ALPHAG'">
          <xsl:attribute name="data-els-type" select="'lower-alpha'"/>
        </xsl:when>
        <xsl:when test="@PRES = 'ALPHAM'">
          <xsl:attribute name="data-els-type" select="'upper-alpha'"/>
        </xsl:when>
        <xsl:when test="@PRES = ('NOM', 'NOGR', 'NOOGR', 'NOO')">
          <xsl:attribute name="data-els-type" select="'decimal'"/>
        </xsl:when>
        <xsl:when test="@PRES = ('NOPAR', 'NOPARGR')">
          <xsl:attribute name="data-els-type" select="'decimal'"/>
          <xsl:attribute name="data-els-sep" select="')'"/>
        </xsl:when>
        <xsl:when test="@PRES = 'NOPOINTGR'">
          <xsl:attribute name="data-els-type" select="'decimal'"/>
          <xsl:attribute name="data-els-sep" select="'.'"/>
        </xsl:when>
        <xsl:when test="@PRES = 'NOTIRGR'">
          <xsl:attribute name="data-els-type" select="'decimal'"/>
          <xsl:attribute name="data-els-sep" select="'-'"/>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates mode="#current"/>
    </ol>
  </xsl:template>
  
  <xsl:template match="LST/ITEM" mode="he:harmonize-efl-focus.content">
    <li>
      <xsl:apply-templates mode="#current"/>
    </li>
  </xsl:template>
  
  <!--=======================================-->
  <!--ENCART-->
  <!--=======================================-->
  
  <xsl:template match="ATTENTION | ESSENTIEL | PRECI" mode="he:harmonize-efl-focus.content">
    <div class="els-block {lower-case(local-name())}">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </div>
  </xsl:template>
  
  <xsl:template match="TESSENTIEL | TPRECI" mode="he:harmonize-efl-focus.content">
    <h4>
      <xsl:apply-templates mode="#current"/>
    </h4>
  </xsl:template>
  
  <!--=======================================-->
  <!--TABLEAUX-->
  <!--=======================================-->
  
  <!--Structure trouvées dans les données : TABLEAU (TGROUP+)--> 
  
  <!--poser le namespace cals automatiquement-->
  <xsl:param name="xslLib:cals2html.set-cals-ns" select="true()" as="xs:boolean"/>
  <!--les multiple tgroup s'affichent bien dans un seul tableau en front-->
  <xsl:param name="xslLib:cals2html.forceMergingMultipleTgroup" select="true()" as="xs:boolean"/>
  
  <xsl:template match="TABLEAU" mode="he:harmonize-efl-focus.content">
    <xsl:if test="* except TGROUP">
      <xsl:sequence select="he:schematron-check('TABLEAU avec autre chose que des TGROUP comme enfant')"/>
    </xsl:if>
    <xsl:if test="@FILET = '1' and @FRAME != 'ALL'">
      <xsl:sequence select="he:schematron-check('TABLEAU CALS avec @FILET=1 and @FRAME!=ALL')"/>
    </xsl:if>
    <xsl:if test="@FILET = '0' and @FRAME != 'NONE'">
      <xsl:sequence select="he:schematron-check('TABLEAU CALS avec @FILET=0 and @FRAME!=NONE')"/>
    </xsl:if>
    <!--Nettoyage tableau avant conversion-->
    <xsl:variable name="table.cals" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="." mode="he:harmonize-efl-focus.clean-cals"/>
      </xsl:document>
    </xsl:variable>
    <!--Conversion de la structure CALS en HTML-->
    <xsl:variable name="table.html" >
      <xsl:apply-templates select="$table.cals" mode="xslLib:cals2html"/>
    </xsl:variable>
    <!--Conversion du contenu des cellules-->
    <xsl:apply-templates select="$table.html" mode="#current"/>
  </xsl:template>
  
  <!--Après conversion cals-to-html, conserver le html des tableaux, en laissant le contenu être traité-->
  <xsl:template match="h:*" mode="he:harmonize-efl-focus.content">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="he:harmonize-efl-focus.content"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="h:div[@class = 'cals_table']" mode="he:harmonize-efl-focus.content" priority="1">
    <div class="els-table">
      <xsl:apply-templates select="@* | node()" mode="he:harmonize-efl-focus.content"/>
    </div>
  </xsl:template>
  
  <!--===========-->
  <!--he:harmonize-efl-focus.clean-cals-->
  <!--===========-->
  
  <xsl:template match="node() | @*" mode="he:harmonize-efl-focus.clean-cals">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="TABLEAU" mode="he:harmonize-efl-focus.clean-cals">
    <TABLE>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </TABLE>
  </xsl:template>

  <!--Suppression de FILET car FRAME est suffisant-->
  <xsl:template match="TABLEAU/@FILET" mode="he:harmonize-efl-focus.clean-cals"/>
  
  <!--===========================-->
  <!--LINKS -->
  <!--===========================-->
  
  <!--
    RefDoc résolues (par MK-Link). Ex: 
    <REFDOC2 juri:cible="texte" ID="R1B874C06DBE1D7E-EFL" ITAL="N" NATURE="C. trav." REF="R 3252-48">
      <LINK COL="BU" FILE="ctra.xml" LINKID="CTRA155440" SUPP="CTRAV"/>
    </REFDOC2>
    <REFDOC2 juri:cible="jp" juri:date="25/03/2009" juri:juridiction="Cass." juri:formation="soc." juri:numero="07-44 925" DATE="25/03/2009" ID="R34074C06DBE1D7E-EFL" ITAL="N" NATURE="Cass. soc." REF="07-44 925">
      <LINK COL="BU" FILE="jpj200903.sgm" LINKID="IFF55E7AB71IC416-EFL" SUPP="JPJ">Cass. soc. 25-3-2009 n° 07-44.925 F-D</LINK>
    </REFDOC2>
  -->
  <xsl:template match="REFDOC2[LINK]" mode="he:harmonize-efl-focus.content">
    <xsl:variable name="self" as="element()" select="."/>
    <xsl:choose>
      <xsl:when test="text()[normalize-space(.)]">
        <xsl:sequence select="he:schematron-check('Texte non vide trouvé dans REFDOC2 résolu')"/>
      </xsl:when>
      <xsl:when test="count(LINK) != 1">
        <xsl:sequence select="he:schematron-check(concat(count(LINK), ' LINK trouvé(s) dans REFDOC2'))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="LINK">
          <!--1 seul LINK mais on utilise for-each pour se positionner dessus (<xsl:copy select="LINK"> en XSLT 3.0)-->
          <xsl:for-each select="LINK">
            <xsl:copy>
              <!--On copie les attributs de la REFDOC2-->
              <xsl:copy-of select="$self/@*"/>
              <xsl:if test="some $att in $self/@* satisfies name($att) = LINK/@*/name(.)">
                <xsl:sequence select="he:schematron-check('Des attributs ont le même nom entre REFDOC2 et REFDOC2/LINK')"/>
              </xsl:if>
              <xsl:copy-of select="@* | node()"/>
            </xsl:copy>
          </xsl:for-each>
        </xsl:variable>
        <xsl:apply-templates select="$LINK" mode="#current"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--FIXME : les liens résolus (links) ont perdu leur attributs de ciblage après le MKLINK, à conserver ?--> 
  <xsl:template match="LINK[@LINKID]" mode="he:harmonize-efl-focus.content">
    <a href="{@LINKID}">
      <xsl:attribute name="class">
        <xsl:if test="@COL = 'BU'">
          <xsl:text>els-link</xsl:text>
        </xsl:if>
        <xsl:text> </xsl:text>
        <xsl:sequence select="concat('elf-', lower-case(local-name()))"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* except @LINKID | node()" mode="#current"/>
    </a>
  </xsl:template>
  
  <xsl:template match="LINK/@*" mode="he:harmonize-efl-focus.content">
    <xsl:attribute name="data-efl-{lower-case(local-name())}" select="."/>
  </xsl:template>
  
  <xsl:template match="LINK[not(@LINKID)]" mode="he:harmonize-efl-focus.content">
    <xsl:sequence select="he:schematron-check('LINK sans @LINKID')"/>
  </xsl:template>
  
  <!--
    RefDoc non-résolues (par MK-Link). Ex:
    <REFDOC2 juri:cible="jp" juri:date="04/11/2011" juri:juridiction="CA" juri:lieu="Dijon" DATE="04/11/2011" ID="R8671C6BD6A1FD5-EFL" ITAL="N" NATURE="CA" REF="Dijon">CA Dijon 4-11-2011 n° 10/01090</REFDOC2>
    <REFDOC2 juri:cible="texte" ID="R1B074C06DBE1D7E-EFL" ITAL="N" NATURE="C. trav." REF="R 3252-12 S/>
  -->
  <xsl:template match="REFDOC2[not(LINK)]" mode="he:harmonize-efl-focus.content">
    <xsl:if test="not(text()[normalize-space(.)])">
      <xsl:sequence select="he:schematron-check('REFDOC2 vide', 'warning')"/>
    </xsl:if>
    <xsl:choose>
      <xsl:when test="@juri:cible = 'jp'">
        <a class="els-link els-jp">
          <xsl:apply-templates select="@* | node()" mode="#current"/>
        </a>
      </xsl:when>
      <xsl:when test="@juri:cible = 'texte'">
        <a class="els-link els-texte">
          <xsl:apply-templates select="@* | node()" mode="#current"/>
        </a>
      </xsl:when>
      <xsl:otherwise>
        <a class="els-link efl-unknown" href="{@LINKID}">
          <xsl:sequence select="he:schematron-check('Lien type inconnu', 'warning')"/>
          <xsl:apply-templates select="@* | node()" mode="#current"/>
        </a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--déja spécifié dans la class-->
  <xsl:template match="REFDOC2/@juri:cible" mode="he:harmonize-efl-focus.content" priority="2"/>
  
  <xsl:template match="REFDOC2/@juri:*" mode="he:harmonize-efl-focus.content" priority="1">
    <!--RDFa rel="inv" ?-->
    <xsl:attribute name="data-els-{lower-case(local-name(.))}" select="."/>
  </xsl:template>
  
  <xsl:template match="REFDOC2/@*" mode="he:harmonize-efl-focus.content">
    <xsl:attribute name="data-efl-{lower-case(local-name(.))}" select="."/>
  </xsl:template>
  
  <!--===========================-->
  <!--TYPO -->
  <!--===========================-->
  <!--DTD : -->
  <!--<!ELEMENT AL 	%STARTENDTAG; (%M.AL;)*>-->
  <!--<!ENTITY % M.AL 	"#PCDATA|E|I|INDEX|MOTREP|MOTVAL|ITAL|UNDERLINE1|UNDERLINE2|
    REFL|RBULL|RFR|RRJ|RMEM|RCOMPTE|RPRAT|FOND|REFDOC2|REFDOCETROIT|RTX|
    LST|RTAB|TABLEAU|RFIG|RF|FORM|FRAC|TABLEUR|NOTE|RNOTE|math|URL|ANCIEN|
    NOUVEAU|STABILO|PDC|TTCBD|TTCBE|TTCBF|LINK|COUPE">-->
  
  <xsl:template match="MOTREP" mode="he:harmonize-efl-focus.content">
    <strong><xsl:apply-templates mode="#current"/></strong>
  </xsl:template>
  
  <xsl:template match="ITAL" mode="he:harmonize-efl-focus.content">
    <em><xsl:apply-templates mode="#current"/></em>
  </xsl:template>
  
  <xsl:template match="E" mode="he:harmonize-efl-focus.content">
    <sup><xsl:apply-templates mode="#current"/></sup>
  </xsl:template>
  
  <xsl:template match="I" mode="he:harmonize-efl-focus.content">
    <sub><xsl:apply-templates mode="#current"/></sub>
  </xsl:template>
  
  <xsl:template match="COUPE" mode="he:harmonize-efl-focus.content">
    <br/>
  </xsl:template>
  
  <!--=========================================================================-->
  <!--COMMON-->
  <!--=========================================================================-->
  
  <xsl:template match="@ID" mode="he:harmonize-efl-focus.content">
    <xsl:attribute name="id" select="."/>
  </xsl:template>
  
  <xsl:function name="els:normalize-ID_LOG" as="xs:string">
    <xsl:param name="s" as="xs:string"/>
    <xsl:sequence select="lower-case(els:normalize-no-diacritic(replace($s, '\p{Z}', '_')))"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Normalize the string: remove diacritic marks.</xd:p>
      <xd:p>Example: els:normalize-no-diacritic('éêèàœç')='eeeaœc'</xd:p>
    </xd:desc>
    <xd:param name="string"/>
    <xd:return>the <xd:b>string</xd:b> normalized</xd:return>
  </xd:doc>
  <xsl:function name="els:normalize-no-diacritic" as="xs:string">
    <xsl:param name="s" as="xs:string"/>
    <xsl:sequence select="replace(normalize-unicode($s, 'NFD'), '[\p{M}]', '')"/>
  </xsl:function>
  
</xsl:stylesheet>